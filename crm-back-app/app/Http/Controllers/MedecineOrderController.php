<?php

namespace App\Http\Controllers;

use App\Models\MedecineOrder;
use Illuminate\Http\Request;

class MedecineOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedecineOrder  $medecineOrder
     * @return \Illuminate\Http\Response
     */
    public function show(MedecineOrder $medecineOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MedecineOrder  $medecineOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(MedecineOrder $medecineOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MedecineOrder  $medecineOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MedecineOrder $medecineOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedecineOrder  $medecineOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedecineOrder $medecineOrder)
    {
        //
    }
}
