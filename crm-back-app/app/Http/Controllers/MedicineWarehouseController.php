<?php

namespace App\Http\Controllers;

use App\Models\MedicineWarehouse;
use Illuminate\Http\Request;

class MedicineWarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicineWarehouse  $medicineWarehouse
     * @return \Illuminate\Http\Response
     */
    public function show(MedicineWarehouse $medicineWarehouse)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MedicineWarehouse  $medicineWarehouse
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicineWarehouse $medicineWarehouse)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MedicineWarehouse  $medicineWarehouse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MedicineWarehouse $medicineWarehouse)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedicineWarehouse  $medicineWarehouse
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicineWarehouse $medicineWarehouse)
    {
        //
    }
}
